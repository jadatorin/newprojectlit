import { LitElement, html, css } from "lit";

export class NewComponent extends LitElement {
  static styles = [
    css`
      :host {
        display: block;
        background: pink;
      }
    `,
  ];

  render() {
    return html`<div>
      <p>Un nuevo parrafo!!</p>
    </div>`;
  }
}
customElements.define("new-component", NewComponent);
