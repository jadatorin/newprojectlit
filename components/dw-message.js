import { LitElement, html, css } from "lit";

class DwMessage extends LitElement {
  static styles = css`
    :host {
      display: inline-block;
      border: 1px solid red;
      padding: 10px;
      background-color: #fcc;
    }
    div {
      display: inline-block;
      border-radius: 0.5rem;
      padding: 0.2rem 0.5rem;
      background-color: #475119;
      color: #fff;
      font-weight: bold;
    }
    p {
      margin-botton: 0;
      font-weight: bold;
    }
  `;

  render() {
    return html`<div>Hola Lit</div>
      <p>Esto es un parrafo</p>`;
  }
}

customElements.define("dw-message", DwMessage);
